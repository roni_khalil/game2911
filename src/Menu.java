import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.LineBorder;

public class Menu {
	private JPanel mainPanel;
	private JPanel playerSelect;
	private JPanel endPanel;
	private JPanel endOptions;
	private JPanel difficultySelect;
	private JPanel maze;
	private CardLayout c;
	private int playerNum;
	private Player playerA;
	private Player playerB;
	private int difficulty;
	private int height;
	private int width;
	private Game g;
	JLabel[][] grid;
	
	private int endAx;
	private int endAy;
	private int endBx;
	private int endBy;
	private int px;
	private int py;
	private int px1;
	private int py1;
	
	//Subject to change
	public Menu(Game initG) {
		playerNum = 1;
		difficulty = 1;
		height = 450;
		width = 500;
		g = initG;
		
		px = 2*g.returnStartY() + 1;
		py = 2*g.returnStartX()+ 1;
		int[] ending = g.returnEnd();
		px1 = ending[0];
		py1 = ending[1];
		endAx = px1;
		endAy = py1;
		endBx = px;
		endBy = py;
		
		
		initUI();
	}
	
	private void initUI(){
		// Create main panels
		playerSelect = getPlayerSelect();
		difficultySelect = getDifficultySelect();
		//maze = mazeScreen(); //only been create when player finish menu
								//see the add addDifficultyButtons()
		
		//Construct Frame to hold panels
		JFrame frame = new JFrame();
		mainPanel = new JPanel();
		c = new CardLayout();
		mainPanel.setLayout(c);
		mainPanel.add(playerSelect, "1");
		mainPanel.add(difficultySelect, "2");
		//mainPanel.add(maze, "3");
		c.show(mainPanel, "1");
		frame.add(mainPanel);
		
		// Define properties for the frame
		frame.setSize(new Dimension(width, height));
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setResizable(false);
		//start in the center of sceen
		frame.setLocationRelativeTo(null);
	}
	
	private void endUI() throws IOException{
		JFrame frame = new JFrame("You made it mah man");
		endPanel = new JPanel();
	
		JLabel label = new JLabel();
		label.setText("<html><h1>A+ you made it</h1></html>");
		label.setBounds(150, -200, 200,500);		
		
		//endPanel.setLayout(null);
		JLabel background = new JLabel(new ImageIcon(ImageIO.read(new File("you-win.png"))));
		frame.setContentPane(background);
		endPanel.add(label);
		endPanel.setBorder(new LineBorder(Color.BLACK));
		
		
		//, new GridBagConstraints()
		frame.add(endPanel);
		
		frame.setSize(new Dimension(width, height));
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		
		frame.setResizable(false);
		frame.setLocationRelativeTo(null);
	}
	
	
	private JPanel getPlayerSelect(){
		JPanel panel = new DrawPanel();
		//panel.setLayout(new FlowLayout());
		panel.setLayout(null);
		addPlayerButtons(panel);
		
		//panel.setBackground(Color.red);
		
		return panel;
	}
	
	
	//background picture
	public class DrawPanel extends JPanel {
	    private BufferedImage image;

	    public DrawPanel() {
	       try {                
	          image = ImageIO.read(new File("theMaze.jpg"));
	       } catch (IOException e) {
	    	   e.printStackTrace();
	       }
	    }

	    @Override
	    protected void paintComponent(Graphics g) {
	        super.paintComponent(g);
	        g.drawImage(image, 0, 0, null); // see javadoc for more info on the parameters            
	    }
	}
	private void addPlayerButtons(JPanel panel){
		ImageIcon icon1 = createImageIcon("/1player.png","1 Player");
		ImageIcon icon2 = createImageIcon("/2player.png","2 Player");
		
		JButton b1 = new JButton("1 Player", icon1);
		b1.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				playerNum = 1;
				playerA = new Player(py,px,g.returnMaze(),1);
				c.show(mainPanel, "2");
			}
		});
		panel.add(b1);
		b1.setBounds(150, 150, 200, 60);
		setButtonAppearance(b1);
		
		
		JButton b2 = new JButton ("2 Player", icon2);
		b2.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				playerNum = 2;
				playerA = new Player(py,px,g.returnMaze(),1);
				playerB = new Player(py1,px1,g.returnMaze(),2);
				c.show(mainPanel, "2");
			}
		});
		panel.add(b2);
		setButtonAppearance(b2);
		b2.setBounds(150, 220, 200, 60);
	}
	
	protected ImageIcon createImageIcon(String path, String description) {
		java.net.URL imgURL = Menu.class.getResource(path);
		if (imgURL != null) {
			return new ImageIcon(imgURL, description);
		} else {
			System.err.println("Couldn't find file: " + path);
			return null;
		}
	}
	
	private JPanel getDifficultySelect(){
		JPanel panel = new DrawPanel();
		//panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		panel.setLayout(null);
		addDifficultyButtons(panel);
		
		//panel.setBackground(Color.green);
		
		return panel;
	}
	
	// TODO change action listeners to go to maze object instead of back to first menu
	private void addDifficultyButtons(JPanel panel){
		//JPanel firstSet = new DrawPanel();
		//firstSet.setBackground(Color.green);
		//firstSet.setLayout(new FlowLayout());
		ImageIcon iconE = createImageIcon("/easy.png","Easy");
		ImageIcon iconM = createImageIcon("/medium.png","Medium");
		ImageIcon iconH = createImageIcon("/hard.png","Hard");
		
		JButton b1 = new JButton("Easy", iconE);
		b1.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				difficulty = 1;
				//Create maze panel because maze panel include timer
				//timer only count time when maze panel been create
				maze = mazeScreen();
				mainPanel.add(maze, "3");
				c.show(mainPanel, "3");
			}
		});
		panel.add(b1);
		setButtonAppearance(b1);
		b1.setBounds(150, 150, 200, 60);
		
		
		JButton b2 = new JButton ("Medium", iconM);
		b2.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				difficulty = 2;
				//Create maze panel because maze panel include timer
				//timer only count time when maze panel been create
				maze = mazeScreen();
				mainPanel.add(maze, "3");
				c.show(mainPanel, "3");
			}
		});
		panel.add(b2);
		setButtonAppearance(b2);
		b2.setBounds(150, 220, 200, 60);
		
		JButton b3 = new JButton ("Hard", iconH);
		b3.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				difficulty = 3;
				//Create maze panel because maze panel include timer
				//timer only count time when maze panel been create
				maze = mazeScreen();
				mainPanel.add(maze, "3");
				c.show(mainPanel, "3");
			}
		});
		panel.add(b3);
		setButtonAppearance(b3);
		b3.setBounds(150, 290, 200, 60);
		
		//panel.add(firstSet);
		
		JButton b4 = new JButton("Back");
		b4.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e){
				c.show(mainPanel, "1");
			}
		});
		panel.add(b4);
		setButtonAppearance(b4);
		b4.setBounds(375, 350, 100, 50);
	}
	
	private void setButtonAppearance(final JButton b) {
		
		//b.setOpaque(false);
		//b.setContentAreaFilled(false);
		//b.setBorderPainted(false);
		b.setBackground(Color.green);
		b.setFont(new Font("Segou UI", Font.BOLD, 20));
		//b.setForeground(Color.WHITE);
		
		b.addMouseListener(new java.awt.event.MouseAdapter() {
		    public void mouseEntered(java.awt.event.MouseEvent evt) {
		        b.setBackground(Color.cyan);
		        //b.setOpaque(true);
				//b.setContentAreaFilled(true);
				//b.setBorderPainted(true);
		    }

		    public void mouseExited(java.awt.event.MouseEvent evt) {
		        //b.setOpaque(false);
		    	b.setBackground(Color.green);
				//b.setContentAreaFilled(false);
				//b.setBorderPainted(false);
				
		    }
		    
		});
		
	}
	
	private JPanel mazeScreen(){
		JPanel mazeScreen = new JPanel();
		mazeScreen.setLayout(new FlowLayout());
		mazeScreen.add(createMaze(9, 13));
		mazeScreen.add(createControls());
		
		return mazeScreen;
	}
	
	private JPanel createControls(){
		   JPanel controlPanel = new JPanel();
		   controlPanel.setPreferredSize(new Dimension(400, 400));
			
		   //Timer Label
		   JLabel timerLabel = new JLabel("Time: 0:0:0", JLabel.CENTER);
		   controlPanel.add(timerLabel);

//		   TimerListener listener = new TimerListener(timerLabel);
		   final Timer timer = new Timer(1000,null);
//		   timer.addActionListener(listener);
		   timer.start();

		   //Continue
		   JButton continueButton = new JButton("Continue");
		   continueButton.addActionListener(new ActionListener(){
			   public void actionPerformed(ActionEvent e){
				   	timer.start();
			   }
		   });
		   controlPanel.add(continueButton);
		   
		   //Pause
		   JButton stopButton = new JButton("Pause");
		   stopButton.addActionListener(new ActionListener(){
			   public void actionPerformed(ActionEvent e){
				   	timer.stop();
			   }
		   });
		   controlPanel.add(stopButton);
		   
		   //Exit button
		   JButton exitButton = new JButton("Exit");
		   exitButton.addActionListener(new ActionListener(){
			   public void actionPerformed(ActionEvent e){
				   c.show(mainPanel, "1");
				   timer.stop();
			   }
		   });
		   controlPanel.add(exitButton);
			
		   controlPanel.setBorder(BorderFactory.createLineBorder(Color.black));
		   
		   return controlPanel;
	   }
	
	private JPanel createMaze(int sizex, int sizey){
	      JPanel mazePanel = new JPanel();
	      mazePanel.setLayout(new GridLayout(sizex, sizey));
	      
	      //Grid stuff
	      grid = new JLabel[sizex][sizey];
	      for(int x = 0; x < sizex; x++){
	    	  for(int y = 0; y < sizey; y++){
	    		  // TODO remove these lines given random values to grid, change to actual maze values
	    		  JLabel add = new JLabel();
	    		  if(g.wallAtMaze(x, y)) add.setBackground(Color.BLACK);
	    		  else add.setBackground(Color.WHITE);
	    		  add.setOpaque(true);
	    		  
	    		  add.setHorizontalAlignment(SwingConstants.CENTER);
	    		  add.setPreferredSize(new Dimension(40, 40));
	    		  
	    		  grid[x][y] = add;
	    		  mazePanel.add(add);
	    	  }
	      }
	      
	      grid[px][py].setBackground(Color.BLUE);
	      grid[endAx][endAy].setBackground(Color.YELLOW);
	      if(playerNum == 2){
		      grid[px1][py1].setBackground(Color.RED);  
	      }
	      
	      //Define some key bindings for this panel
	      addKeyboardEvents(mazePanel);
	      return mazePanel;
	}
	
	private void addKeyboardEvents(JPanel p){
	    //Player 1
  		p.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("UP"), "up1");
  	    p.getActionMap().put("up1", new KeyboardEvent("UP"));
  	    p.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("DOWN"), "down1");
  	    p.getActionMap().put("down1", new KeyboardEvent("DOWN"));
  	    p.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("LEFT"), "left1");
  	    p.getActionMap().put("left1", new KeyboardEvent("LEFT"));
  	    p.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("RIGHT"), "right1");
  	    p.getActionMap().put("right1", new KeyboardEvent("RIGHT"));
  	    
		//Player 2
		p.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("W"), "up2");
	    p.getActionMap().put("up2", new KeyboardEvent("W"));
	    p.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("S"), "down2");
	    p.getActionMap().put("down2", new KeyboardEvent("S"));
	    p.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("A"), "left2");
	    p.getActionMap().put("left2", new KeyboardEvent("A"));
	    p.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke("D"), "right2");
	    p.getActionMap().put("right2", new KeyboardEvent("D"));
	}
	
	private void testMove(String dir){		
		grid[px][py].setBackground(Color.WHITE);
		grid[px][py].repaint();
		
		grid[endAx][endAy].setBackground(Color.YELLOW);
		grid[endAx][endAy].repaint();
		
		if(playerNum == 2) {
			grid[px1][py1].setBackground(Color.WHITE);
			grid[px1][py1].repaint();
			
			grid[endBx][endBy].setBackground(Color.GREEN);
			grid[endBx][endBy].repaint();
		}
			
		//TODO test if valid move
		//playerA
		if(dir.equals("UP")){
			if (playerA.checkVild(g.returnMaze(), 0) == true){
				playerA.playerMove(g.returnMaze(), 0);
			}
		}
		if(dir.equals("DOWN")){
			if (playerA.checkVild(g.returnMaze(), 2) == true){
				playerA.playerMove(g.returnMaze(), 2);

			}
		}
		if(dir.equals("LEFT")){
			if (playerA.checkVild(g.returnMaze(), 3) == true){
				playerA.playerMove(g.returnMaze(), 3);
			}
		} 
		if(dir.equals("RIGHT")){
			if (playerA.checkVild(g.returnMaze(), 1) == true){
				playerA.playerMove(g.returnMaze(), 1);
			}
		}
		py = playerA.getHoriPos();
		px = playerA.getVertPos();
		if(g.isAtFinish(px, py) && playerNum == 1)
			try {
				endUI();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}


		//playerB
		if(playerNum == 2){
			//playerB
			if(dir.equals("W")){
				if (playerB.checkVild(g.returnMaze(), 0) == true){
					playerB.playerMove(g.returnMaze(), 0);
				}
			}
			if(dir.equals("S")){

				if (playerB.checkVild(g.returnMaze(), 2) == true){
					playerB.playerMove(g.returnMaze(), 2);
				}
			}
			if(dir.equals("A")){
				if (playerB.checkVild(g.returnMaze(), 3) == true){
					playerB.playerMove(g.returnMaze(), 3);
				}
			} 
			if(dir.equals("D")){

				if (playerB.checkVild(g.returnMaze(), 1) == true){
					playerB.playerMove(g.returnMaze(), 1);
				}
			}
			py1 = playerB.getHoriPos();
			px1 = playerB.getVertPos();
			if(g.isAtFinish(px, py)) System.out.println("I'm at the finish line! P1");
			if(px1 == 2*g.returnStartY()+1 && py1 == 2*g.returnStartX()+1) System.out.println("I'm at the finish line! P2");
			
			grid[endAx][endAy].setBackground(Color.YELLOW);
			grid[endAx][endAy].repaint();
			grid[px1][py1].setBackground(Color.RED);
			grid[px1][py1].repaint();
		}
		
		grid[px][py].setBackground(Color.BLUE);
		grid[px][py].repaint();
	}
	
	@SuppressWarnings("serial")
	public class KeyboardEvent extends AbstractAction{
		private String pressed;

		
		public KeyboardEvent(String initPressed){
			super();
			pressed = initPressed;
		}
	
		@Override
		public void actionPerformed(ActionEvent e) {
			g.handleInput(pressed);
			testMove(pressed);
		}


	}

}