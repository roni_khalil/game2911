import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JLabel;

public class TimerListener implements ActionListener{

    private int sec = 0;
    private int min = 0;
    private int hr = 0;

    private JLabel label;

    public TimerListener(JLabel label){
        this.label = label;
    }

    public void actionPerformed(ActionEvent e) {

        if (sec > 59){
        	sec = 0;
        	min++;
        } else {
        	sec++;
        }
        
        if (min > 59){
        	min = 0;
        	hr++;
        }
        
        if (hr > 59){
        	hr = 0;
        }

        label.setText("Time: " + hr + ":" + min + ":" + sec);
    }
}