
public class Cell {
	private int x;
	private int y;
	private boolean visited;
	
	public Cell (int x, int y){
		
		this.visited = false;
		this.x = x;
		this.y = y;
	}
	
	
	public int returnX(){
		return this.x;
	}
	
	public int returnY(){
		return this.y;
	}
	
	public boolean returnVisited(){
		return this.visited;
	}
	
	public void plusMinusY(int direction){
		if(direction == 0) this.y++;
		if(direction == 1) this.y--;
	}
	
	public void plusMinusX(int direction){
		if(direction == 0) this.x++;
		if(direction == 1) this.x--;
	}
	
	public void setVisited(){
		this.visited = true;
	}
	
}
