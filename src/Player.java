
public class Player {
	private static final int North = 0;
	private static final int East = 1;
	private static final int South = 2;
	private static final int West = 3;
	
	private int horiPos;
	private int vertPos;
	private int playerNo;
	
	public Player(int horiPos, int vertPos, int[][] maze, int playerNumber){
		this.horiPos = horiPos;
		this.vertPos = vertPos;
		this.playerNo = playerNumber + 2;
		//maze[vertPos][horiPos] = this.playerNo;
	}
	
	public int getHoriPos(){
		return this.horiPos;
	}
	
	public int getVertPos(){
		return this.vertPos;
	}
	
	public boolean checkVild(int[][] getMaze, int direction){
		switch(direction){
			case North:
				if(getMaze[this.vertPos-1][this.horiPos] == 1){
					return true;
				} else {
					return false;
				}
			case East:
				if(getMaze[this.vertPos][this.horiPos+1] == 1){
					return true;
				} else {
					return false;
				}
			case South:
				if(getMaze[this.vertPos+1][this.horiPos] == 1){

					return true;
				} else {
					return false;
				}
			case West:
				if(getMaze[this.vertPos][this.horiPos-1] == 1){
					return true;
				} else {
					return false;
				}
		}
		return false;
	}
	
	public void playerMove(int[][] getMaze, int direction){
		//getMaze[this.vertPos][this.horiPos] = 0;
		switch(direction){
		case North:
			this.vertPos -= 1;
			break;
		case East:
			this.horiPos += 1;
			break;
		case South:
			this.vertPos += 1;
			break;
		case West:
			this.horiPos -= 1;
			break;
		}

		
		//getMaze[this.vertPos][this.horiPos] = this.playerNo;
	}
}
