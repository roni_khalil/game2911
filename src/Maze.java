import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Random;

public class Maze {
	private int height = 4;
	private int width = 6;
	private int startX;
	private int startY;
	private boolean[] hWall;
	private boolean[] vWall;
	private int[][] finalArray;
	private Random rand;
	private int[] endPoint;
	
	private static final int North = 0;
	private static final int East = 1;
	private static final int South = 2;
	private static final int West = 3;
	
	public Maze(){
		hWall = new boolean[((this.width + 1)*(this.height+1))*2];
		Arrays.fill(hWall, true);
		vWall = new boolean[((this.height + 1)*(this.width+1))*2];
		Arrays.fill(vWall, true);
		rand = new Random();
		this.startX = rand.nextInt(this.width);
		this.startY = rand.nextInt(this.height);
		this.finalArray = new int[this.height*2+1][this.width*2+1];
	}

	public void generator(){
		Cell start = new Cell(this.startX, this.startY);
		LinkedList<Cell> stack = new LinkedList<Cell>();
		stack.addFirst(start);
		int[] neighbours = new int[4];
		
		boolean[] visited = new boolean[(this.width*this.height + this.width*this.height)*2];
		while(!stack.isEmpty()){
			visited[start.returnY()*this.width + start.returnX()] = true;
			
			//get all neighbours for current Cell
			int neighbourCount = 0;
			for(int i = 0; i < 4; i++){
				
				switch(i){
					
					case North:
						if(start.returnY() > 0 && !visited[(start.returnY() - 1)*this.width + start.returnX()]){
							neighbours[neighbourCount++] = i;
						}
						break;
					case East:
						if(start.returnX() < this.width - 1 && !visited[start.returnY()*this.width + (start.returnX() + 1)]){
							neighbours[neighbourCount++] = i;
						}
						break;
					case South:
						if(start.returnY() < this.height - 1 && !visited[(start.returnY()+1)*this.width + start.returnX()]){
							neighbours[neighbourCount++] = i;
						}
						break;
					case West:
						if(start.returnX() > 0 && !visited[start.returnY()*this.width + (start.returnX() - 1)]){
							neighbours[neighbourCount++] = i;
						}
						break;
				}
			}
			//select new random neighbour to go to
			if(neighbourCount > 0){
				stack.addFirst(start);
				start = new Cell(start.returnX(), start.returnY());
				
				//randomise the neighbour chosen
				switch(neighbours[rand.nextInt(neighbourCount)]){
					
					case North:
						carvePath(start.returnX(), start.returnY(), North);
						start.plusMinusY(1);
						break;
					case East:
						carvePath(start.returnX(), start.returnY(), East);
						start.plusMinusX(0);
						break;
					case South:
						carvePath(start.returnX(), start.returnY(), South);
						start.plusMinusY(0);
						break;
					case West:
						carvePath(start.returnX(), start.returnY(), West);
						start.plusMinusX(1);
						break;
				}
				
			} else{
				start = stack.removeFirst();
			}
		}
	}
	
	private boolean carvePath(int x, int y, int Direction){
		
		int index = -1;
		boolean[] check = null;
		switch(Direction){
			case North:
				index = y*this.width + x;
				check = hWall;
				//putting the data into 2D Array
				this.finalArray[2*y+1][2*x+1] = 1;
				this.finalArray[2*y][2*x+1] = 1;
				break;
			case South:
				index = (y + 1)*this.width + x;
				check = hWall;
				this.finalArray[2*y+1][2*x+1] = 1;
				this.finalArray[2*y+2][2*x+1] = 1;
				break;
			case East:
				index = y*(this.width + 1) + x;
				check = vWall;
				this.finalArray[2*y+1][2*x+1] = 1;
				this.finalArray[2*y+1][2*x+2] = 1;
				break;
			case West:
				index = y*(this.width + 1) + (x + 1);
				check = vWall;
				this.finalArray[2*y+1][2*x+1] = 1;
				this.finalArray[2*y+1][2*x] = 1;
				break;
		}

		boolean isWall = check[index];
		check[index] = false;
		return isWall;
	}
	
	public boolean isWall(int xCord, int yCord){
		if(finalArray[xCord][yCord] == 0) return true;
		return false;
	}
	
	public int returnStartX (){
		return this.startX;
	}
	
	public int returnStartY(){
		return this.startY;
	}
	
	public int[][] returnMaze(){
		return this.finalArray;
	}
	
	public void findEndPoint(int x, int y){
		
		// contains the information about the endpoint [x][y][dist]
		endPoint = new int[3];
		// contains the information about the initial starting point
		int[] startPoint = new int[3];
		startPoint[0] = x;
		startPoint[1] = y;
		startPoint[2] = 0;
		
		// places to visit queue
		Queue<int[]> toVisit = new LinkedList<int[]>();
		// already visited nodes
		List<int[]> visitedArray = new ArrayList<int[]>();
		
		// add the initial point into the queue
		toVisit.add(startPoint);
		
		int[] currentPoint = new int[3];
		int[] potentialPoint = new int[3];
		while (!toVisit.isEmpty()) {
			
			// the next node is now the current node
			currentPoint = toVisit.poll();
			System.out.println("Current Point: x = " + currentPoint[0] + " y = " + currentPoint[1]);
			
			// updates endpoint if the current point has a higher distance away from the start than others
			if(currentPoint[2] > endPoint[2]) {
				endPoint = currentPoint;
			}
			
			// checking adjacent points in the graph
			for(int i = -1; i < 2; i = i + 2) {
				
				// checking the horizontal axis
				potentialPoint[0] = currentPoint[0] + i;
				potentialPoint[1] = currentPoint[1];
				potentialPoint[2] = currentPoint[2];
				System.out.println("Potential Point Hori: x = " + potentialPoint[0] + " y = " + potentialPoint[1]);
				checkingPotentialPoint(toVisit, visitedArray, potentialPoint);

				// checking the vertical axis
				potentialPoint[0] = currentPoint[0];
				potentialPoint[1] = currentPoint[1] + i;
				potentialPoint[2] = currentPoint[2];
				System.out.println("Potential Point Vert: x = " + potentialPoint[0] + " y = " + potentialPoint[1]);
				checkingPotentialPoint(toVisit, visitedArray, potentialPoint);
				
			}
			
			// add that point into the visited array
			visitedArray.add(currentPoint);
			
		}
		
		System.out.println("End point is " + endPoint[0] + " , " + endPoint[1]);
	}
	
	private void checkingPotentialPoint(Queue<int[]> toVisit, List<int[]> visitedArray, int[] potentialPoint) {
	
		boolean check = true;
		
		if(potentialPoint[0] < 0 || potentialPoint[1] < 0) {
			System.out.println("Out of Bounds");
			check = false;
		// skip if adjacent pixel is a wall
		} else if(finalArray[potentialPoint[0]][potentialPoint[1]] == 0) {
			System.out.println("Is a Wall");
			check = false;
		} 
		
		// skip if adjacent pixel is already been looked at
		for(int i = 0; i < visitedArray.size(); i++) {
			if(visitedArray.get(i)[0] == potentialPoint[0] && visitedArray.get(i)[1] == potentialPoint[1]) check = false;
		}
		
		// checks whether it satisfies the conditions to put into the queue
		if(check == true) {
			int[] newPoint = new int[3];
			newPoint[0] = potentialPoint[0];
			newPoint[1] = potentialPoint[1];
			newPoint[2] = potentialPoint[2];
			newPoint[2]++;
			System.out.println("New Point: x = " + newPoint[0] + " y = " + newPoint[1]);
			toVisit.add(newPoint);
		}

	}
	
	public int[] getEndPoint() {
		return endPoint;
	}
	
	public void printMatrixMaze(){
		for(int y = 0; y < this.height*2+1; y++){
			for(int x = 0; x < this.width*2+1; x++){
				System.out.print(this.finalArray[y][x]);
			}
			System.out.println("");
		}
	}
}
