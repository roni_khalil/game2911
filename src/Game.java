import javax.swing.SwingUtilities;

public class Game {
	private Menu menu;
	private Maze newMaze;

	public static void main(String[] args){
		/*
		SwingUtilities.invokeLater(new Runnable(){
			public void run(){
				new Menu();
			}
		});
		*/
		Game newGame = new Game();
		//new Menu(newGame);
		//newGame.createMaze();
	}
	
	public Game(){
		createMaze();
		menu = new Menu(this);
	}

	public void createMaze(){
		newMaze = new Maze();//need to input height and width
		newMaze.generator();
		newMaze.printMatrixMaze();
		int startX = 2*newMaze.returnStartY() + 1;
		int startY = 2*newMaze.returnStartX() + 1;
		newMaze.findEndPoint(startX, startY);

	}

	public void handleInput(String pressed) {
		System.out.println("Not yet implemented");
	}
	
	public boolean wallAtMaze(int x, int y){
		if(newMaze != null) return newMaze.isWall(x, y);
		System.out.println("No maze exists");
		return true;
	}
	
	public int[][] returnMaze(){
		return this.newMaze.returnMaze();
	}
	
	public int returnStartX() {
		return this.newMaze.returnStartX();
	}
	
	public int returnStartY() {
		return this.newMaze.returnStartY();
	}
	
	public int[] returnEnd() {
		return this.newMaze.getEndPoint();
	}
	
	public boolean isAtFinish(int x, int y) {
		int[] finish = newMaze.getEndPoint();
		if(x == finish[0] && y == finish[1]) return true;
		return false;
	}
	
}
